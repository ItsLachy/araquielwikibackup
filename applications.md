<!-- TITLE: Real-world Applications -->
<!-- SUBTITLE: A few servers that have demonstrated massive success using Araquiel. -->

# Client 1: Araquiel Hangout
Araquiel Hangout has taken advantage of numerous features in Araquiel.  Some of the features that are used include: 
* Modlogs
* Welcome Messages
* Leveling
* Message Counting
* Role Management
* Image-Only Channels
## Modlogs
![B 7 Fc 0 A 1 A 9 Baf 90163 F 9 F 588641 E 31 Ef 2](/uploads/b-7-fc-0-a-1-a-9-baf-90163-f-9-f-588641-e-31-ef-2.png "B 7 Fc 0 A 1 A 9 Baf 90163 F 9 F 588641 E 31 Ef 2")
## Welcome Messages
![7681342 B 1 B 2 Ca 209822 B 96 Edce 0 Cc 2 D 5](/uploads/7681342-b-1-b-2-ca-209822-b-96-edce-0-cc-2-d-5.png "7681342 B 1 B 2 Ca 209822 B 96 Edce 0 Cc 2 D 5")
## Image-Only Channels
![Rdln 4 Dd 6 Nr](/uploads/rdln-4-dd-6-nr.gif "Rdln 4 Dd 6 Nr")