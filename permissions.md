<!-- TITLE: Permissions -->

# Permissions Introduction
This feature extends the default permission model of the bot. By default, many commands are restricted based on what the command can do.
This feature allows you to refine some of those restrictions. You can allow wider or narrower access to most commands using it. You cannot, however, change the restrictions on owner-only commands.

When additional rules are set using this cog, those rules will be checked prior to checking for the default restrictions of the command.
Global rules (set by the owner) are checked first, then rules set for servers. If multiple global or server rules apply to the case, the order they are checked is:
1. Rules about a user.
2. Rules about the voice channel a user is in.
3. Rules about the text channel a command was issued in.
4. Rules about a role the user has (The highest role they have with a rule will be used).
5. Rules about the server a user is in (Global rules only).


## Examples

Locking the ``/play`` command to approved server(s) as a bot owner:

    /permissions setdefaultglobalrule deny play
    /permissions addglobalrule allow play [server ID or name]

Locking the ``[p]play`` command to specific voice channel(s) as a serverowner or admin:

    /permissions setdefaultserverrule deny play
    /permissions setdefaultserverrule deny "playlist start"
    /permissions addserverrule allow play [voice channel ID or name]
    /permissions addserverrule allow "playlist start" [voice channel ID or name]

Allowing extra roles to use ``/cleanup``:

    /permissions addserverrule allow cleanup [role ID]

Preventing ``/cleanup`` from being used in channels where message history is important:

    /permissions addserverrule deny cleanup [channel ID or mention]
