<!-- TITLE: Araquriel | Wiki — Home -->
<!-- SUBTITLE: A quick summary of Commands -->

# Welcome to Araquiel's Wiki
### Important — The default prefix for Araquiel is ``/``.  You can change this by doing ``/set serverprefix (prefix)``
## Features
* Moderation

# How can I add Araquiel to my server?
You can add Araquiel to your own server with the following link: https://wiki.nexusbot.cf/add

# Music
**Sub Commands**
* now [np] — Now playing.
* pause — Pause and resume.
* percent — Queue percentage.
* play — Play a URL or search for a track.
* playlist — Playlist configuration options.
* prev — Skip to the start of the previously played track.
* queue [q] — List the queue.
* remove — Remove a specific track number from the queue.
* repeat — Toggle repeat.
* search — Pick a track with a search.
* seek — Seek ahead or behind on a track by seconds.
* shuffle — Toggle shuffle.
* skip — Skip to the next track.
* stop — Stop playback and clear the queue.
* volume — Set the volume, 1% - 150%.

# Economy Module
## Bank Module
* daily — Claim your daily reward of $300!
* bal [balance] — Check how much 💵 you have!
* payouts — Show the payouts for the slot machine.
* slot — Use the slot machine.
* leaderboard — Print the leaderboard.

## Casino Module
* allin — Bets all your currency for a chance to win big!
* blackjack — Play a game of blackjack.
* casino — Interacts with the Casino system.
* casinoset — Changes Casino settings
* coin — Coin flip game with a 5050 chance to win.
* craps — Plays a modified version of craps
* cups — Guess which cup of three is hiding the coin.
* dice — Roll a set of dice and win on 2, 7, 11, 12.
* double — Play a game of Double Or Nothing.
* hilo — Pick high, low, or 7 in a dice rolling game.
* war — Play a modified game of war.

## Role Shop
* buyrole — Buy a role. 
* buyroleset — Configuration for buyrole 
* roleprices — Shows prices of roles


# Admin Module
## Core Admin Section
* addrole — Add a role to a user.
* editrole — Edit role settings.
* removerole — Remove a role from a user.
* selfrole — Add a role to yourself.
* sendembed — Sends an embed with specified text.

## Message Filter
*You can use regexes to filter messages, as well as words or sentences. For sentences, you need to use quotes for that to work.*
* filter — Add or remove words from server filter.
* filterset — Manage filter settings.

## Warning System
* actionlist — List all configured automated actions for Warnings.
* reasonlist — List all configured reasons for Warnings.
* unwarn — Remove a warning from a user.
* warn — Warn the user for the specified reason.
* warnaction — Manage automated actions for Warnings.
* warnings — List the warnings for the specified user.
* warningset — Manage settings for Warnings.
* warnreason — Manage warning reasons.



# Interaction Commands
* baka — You baka!
* boop — Boop!
* cry — Cry...
* cuddle — Cuddles a user!
* feed — Feeds a user!
* fortune — What are your chances?
* highfive — Highfives a user!
* hug — Hugs a user!
* kiss — Kiss a user!
* lick — Licks a user!
* neko — Show a neko!
* pat — Pats a user!
* poke — Pokes a user!
* slap — Slaps a user!
* smug — Be smug towards someone!
* tackle — Tackle a user!
* tickle — Tickles a user!
* waifu — Show a waifu!
* wave — Wave at a user!

# Moderation Module
* ban — Ban a user from this server.
* hackban — Pre-emptively ban a user from this server.
* ignore — Add servers or channels to the ignore list.
* kick — Kick a user.
* modset — Manage server administration settings.
* mute — Mute users.
* names — Show previous names and nicknames of a user.
* rename — Change a user's nickname.
* softban — Kick a user and delete 1 day's worth of their messages.
* tempban — Temporarily ban a user from this server.
* unban — Unban a user from this server.
* unignore — Remove servers or channels from the ignore list.
* unmute — Unmute users.
* userinfo — Show information about a user.
* voicekick — Kick a member from a voice channel.
* voiceban — Ban a user from speaking and listening in the server's voice channels.
* voiceunban — Unban a user from speaking and listening in the server's voice channels.
* purge — Delete specified messages [Example: purge 15]

# Leveling Module
* backgrounds — Gives a list of backgrounds. [p]backgrounds [profile|rank|levelup]
* badge — Badge configuration options
* lvladmin — Admin settings.
* lvlinfo — Gives more specific details about a user's profile.
* lvlset — Profile configuration options.
* profile — Displays a user profile.
* rank — Displays the rank of a user.
* rep — Gives a reputation point to a designated player.
* role — Role configuration.
* top — Displays the leaderboard.

# Information Module

* access — Check channel access
* banlist — Displays the server's banlist.
* cid — Shows the channel ID.
* cinfo — Shows channel information. Defaults to current text channel.
* eid — Get an id for an emoji.
* einfo — Emoji information.
* inrole — Check members in the role specified.
* listchannel — List the channels of the current server
* listguilds — List the guilds|servers the bot is in
* newusers — Lists the newest 5 members.
* perms — Fetch a specific user's permissions.
* rid — Shows the id of a role.
* rinfo — Shows role info.
* sid — Show the server ID.
* sinfo — Shows server information.
* uinfo — Shows user information. Defaults to author.
* whatis — What is it?
* getroles —  Displays all roles their ID and number of members in order of
* emoji — Post a large size emojis in chat
* serverinfo — Show server information.
* botstats — Display stats about the bot
* channeledit — Modify channel options
* channelstats — Gets total messages in a specific channel as well as the user who said them.
* getguild — Display info about servers the bot is on
* getreactions — Gets a list of all reactions from specified message and displays the user ID,
* getroles — Displays all roles their ID and number of members in order of
* guildedit — Edit various guild settings
* listchannels — Lists channels and their position and ID for a server
* nummembers — Display number of users on a server
* pruneroles — Perform various actions on users who haven't spoken in x days
* serverstats — Gets total messages on the server and displays each channel.

## Dungeon (Raid-Containment)
* banish Strip a user of their roles, apply the dungeon role, and blacklist them.
* dungeon Main dungeon commands -- Specified below:
	* announce Sets the announcement channel for users moved to the dungeon.
	* autoban Toggle auto-banning users instead of sending them to the dungeon.
	* autosetup Automatically set up the dungeon channel and role to apply to suspicious users.
	* banmessage Set the message to send on an autoban. If message is left blank, no message will be sent.
	* blacklist Toggle auto-blacklisting for the bot for users moved to the dungeon.
	* channel Sets the text channel to use for the dungeon.
	* dm Set the message to send on successful verification.
	* joindays Set how old an account needs to be a trusted user.
	* modlog Toggle using the Red mod-log for auto-bans.
	* profiletoggle Toggles flagging accounts that have a default profile pic.
	* role Sets the role to use for the dungeon.
	* settings Show the current settings.
	* toggle Toggle the dungeon on or off.
	* userrole Sets the role to give to new users that are not sent to the dungeon.
	* usertoggle Toggle the user role on or off.
	* verify Verify a user: remove the dungeon role and add initial user role.

# Role Management
* massrole — Commands for mass role management
* rolebind — Binds a role to a reaction on a message...
* roleset — Settings for role requirements
* roleunbind — unbinds a role from a reaction on a message
* srole — Self assignable role commands

## Araquiel Stats
* bankstats — Show stats of the bank.
* prefix — Show all prefixes of the bot
* servercount — Send servers stats of the bot.
* serversregions — Show total of regions where the bot is.
* usagecount — Show the usage count of the bot.

## Report
* report — Send a report.
* reportset — Manage Reports.

# Fun Module
* dare — Dare someone!
* truth — Ask a truth question to users!
* wouldyourather [wyr] — Would you rather?


## Birthdays
> Birthdays are server-based, not global.
* birthday — Sets your birthday [Example: birthday 12-10 2002]
* birthdays — List birthdays on the server. [Example: birthdays]


## Starboard
* star — Manually star a message using a message ID [Example: star 562988389184569344]
* starboard — Commands for managing the starboard


## Misc Interaction
* guildemojis — Display all server emojis in a menu that can be scrolled through
* avatar [av] — Display a users avatar in chat
* someone — Help I've fallen and I need @someone.
* ship — Calculate the love percentage!


## AFK
* away — Tell the bot you're away or back.
* awaysettings — View your current away settings
* dnd — Set an automatic reply when you're dnd.
* gaming — Set an automatic reply when you're playing a specified game.
* idle — Set an automatic reply when you're idle.
* listening — Set an automatic reply when you're listening to Spotify.
* offline — Set an automatic reply when you're offline.
* streaming — Set an automatic reply when you're streaming.
* toggleaway — Toggle away messages on the whole server.

## Dueling
* duel — Duel another player
* duels — Show your dueling history.
* protect — Manage the protection list (adds items)
* protected — Displays the duel protection list
* unprotect — Manage the protection list 

# Cards Against Humanity
* cahcredits — Code credits.
* cahgames — Displays up to 10 CAH games in progress.
* chat — Broadcasts a message to the other players in your game.
* flushhand — Flushes the cards in your hand - can only be done once per game.
* game — Displays the game's current status.
* hand — Shows your hand.
* idlekick — Sets whether or not to kick members if idle for 5 minutes or more. Can only be done by the player who created the game.
* joinbot — Adds a bot to the game. Can only be done by the player who created the game.
* joinbots — Adds bots to the game. Can only be done by the player who created the game.
* joincah — Join a Cards Against Humanity game. If no id or user is passed, joins a random game.
* laid — Shows who laid their cards and who hasn't.
* lay — Lays a card or cards from your hand. If multiple cards are needed, separate them by a comma (1,2,3).
* leavecah — Leaves the current game you're in.
* newcah — Starts a new Cards Against Humanity game.
* pick — As the judge - pick the winning card(s).
* removebot — Removes a bot from the game. Can only be done by the player who created the game.
* removeplayer — Removes a player from the game. Can only be done by the player who created the game.
* score — Display the score of the current game.


# Image Manipulation
* aesthetics Returns inputed text in aesthetics
* ascii — Convert text into ASCII
* caption — Add caption to an image
* emojify — Replace characters in text with emojis
* flipimg — Rotate an image 180 degrees
* flop — Flip an image
* glitch — Glitch a gif or png
* glitch2 — Glitch a jpegs
* gmagik — Attempt to do magik on a gif
* haah — Mirror an image vertically left to right
* hooh — Mirror an image horizontally bottom to top
* invert — Invert the colours of an image
* jpeg — Add more JPEG to an Image
* magik — Apply magik to Image(s)
* merge — MergeCombine Two Photos
* minecraftachievement — Generate a Minecraft Achievement
* pixelate — Picelate an image
* retro — Create a retro looking image
* retro2 — Create a retro looking image
* retro3 — Create a retro looking image
* rip — Generate tombstone image with name and optional text
* rotate — Rotate image X degrees
* sans — Generate an image of text with comicsans
* tone — Analyze Tone in Text
* triggered — Generate a Triggered Gif for a User or Image
* tti — Generate an image of text
* vw — Add vaporwave flavours to an imag
* waaw — Mirror an image vertically right to left
* watermark — Add a watermark to an image
* woow — Mirror an image horizontally top to bottom

# NSFW Module
## Important: These commands will only work in NSFW channels.

* /4k — Show some 4k images from random subreddits.
* /ahegao — Show some ahegao images from random subreddits.
* /anal — Show some anal images/gifs from random subreddits.
* /ass — Show some ass images from random subreddits.
* /bdsm — Show some bdsm from random subreddits.
* /blackcock — Show some blackcock images from random subreddits.
* /blowjob — Show some blowjob images/gifs from random subreddits.
* /boobs — Show some boobs images from random subreddits.
* /bottomless — Show some bottomless images from random subreddits.
* /cleandm — Delete a number specified of DM's from the bot.
* /cosplay — Show some nsfw cosplay images from random subreddits.
* /cumshot — Show some cumshot images/gifs from random subreddits.
* /cunnilingus — Show some cunnilingus images from random subreddits.
* /deepthroat — Show some deepthroat images from random subreddits.
* /dick — Show some dicks images from random subreddits.
* /doublepenetration — Show some double penetration images/gifs from random subreddits.
* /futa — Show some futa images from random subreddits.
* /gay — Show some gay porn from random subreddits.
* /gonewild — Show some gonewild images from random subreddits.
* /group — Show some groups nudes from random subreddits.
* /hentai — Show some hentai images/gifs from Nekobot API.
* /lesbian — Show some lesbian gifs or images from random subreddits.
* /milf — Show some milf images from random subreddits.
* /oral — Show some oral gifs or images from random subreddits.
* /porngif — Show some porn gifs from Nekobot API.
* /public — Show some public nude images from random subreddits.
* /pussy — Show some pussy nude images from random subreddits.
* /realgirls — Show some real girls images from random subreddits.
* /redhead — Show some red heads images from random subreddits.
* /rule34 — Show some rule34 images from random subreddits.
* /squirt — Show some squirts images from random subreddits.
* /thigh — Show some thighs images from random subreddits.
* /trap — Show some traps from random subreddits.
* /yiff — Show some yiff images from random subreddits.